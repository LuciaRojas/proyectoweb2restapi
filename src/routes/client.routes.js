const express = require("express");
const router = express.Router();
//const verifyToken = require('../controllers/verifyToken')
const {
    renderClientForm,
    createNewClient,
    updateCliente,
    renderClient,
    rendeEditForm,
    deleteClient,
} = require("../controllers/client.controller");

// Routes
// Helpers
const { isAuthenticated } = require("../helpers/auth");

router.get("/clients/add", isAuthenticated,renderClientForm);
//Post Client
router.post("/clients/new-client", isAuthenticated,createNewClient);

// Get All Clients
router.get("/clients", isAuthenticated,renderClient);

// Edit Clients
router.get("/clients/edit/:id", isAuthenticated,rendeEditForm);

router.put("/clients/edit-client/:id", isAuthenticated,updateCliente);
// Delete Client
router.delete("/clients/delete/:id", isAuthenticated,deleteClient);
module.exports = router;
