const express = require("express");
const router = express.Router();

const {
    renderMetingForm,
    createNewMeting,
    updateMeting,
    renderMeting,
    rendeEditForm,
    deleteMeting,
} = require("../controllers/meting.controller");
//metings
// Routes
// Helpers
const { isAuthenticated } = require("../helpers/auth");

router.get("/metings/add",isAuthenticated,renderMetingForm);

//Post meting 
router.post("/metings/new-meting",isAuthenticated,createNewMeting);

// Get All metings
router.get("/metings",isAuthenticated,renderMeting);

// Edit metings
router.get("/metings/edit/:id",isAuthenticated,rendeEditForm);

router.put("/metings/edit-meting/:id",isAuthenticated,updateMeting);
// Delete Client
router.delete("/metings/delete/:id",isAuthenticated,deleteMeting);

module.exports = router;
