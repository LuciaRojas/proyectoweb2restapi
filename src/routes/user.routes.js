const router = require("express").Router();

const {
  renderSignUpForm,
  singup,
  renderSigninForm,
  signin,
  logout,
  renderUser,
  rendeEditForm,
  updateUser,
  deleteUser,
  renderPrincipal
} = require("../controllers/user.controller");

// Helpers
const { isAuthenticated } = require("../helpers/auth");

// Routes

//renderisa al login
router.get("/users/signin", renderSigninForm);

router.post("/users/signin", signin);
//renderisa para salir de todo y volver al login
router.get("/users/logout", logout);
//renderisa al formularioSinUp
router.get("/users/signup" ,isAuthenticated,renderSignUpForm);
// Post  User
router.post("/users/signup",isAuthenticated,singup);
//renderisa a la pagina principal
router.get("/principal" ,isAuthenticated,renderPrincipal);
// Get All Users
router.get("/users" ,isAuthenticated,renderUser);

// Edit User
router.get("/users/edit/:id",isAuthenticated,rendeEditForm);

router.put("/users/edit-user/:id",isAuthenticated,updateUser);
// Delete User
router.delete("/users/delete/:id" ,isAuthenticated,deleteUser);




module.exports = router;
