const express = require("express");
const router = express.Router();

const {
    renderSuportForm,
    createNewSuport,
    updateSuport,
    renderSuport,
    rendeEditForm,
    deleteSuport,
} = require("../controllers/suport.controller");
//metings
// Routes
// Helpers
const { isAuthenticated } = require("../helpers/auth");
// muestra el formulario de insertar soporte
router.get("/suports/add",isAuthenticated,renderSuportForm);

// Post suport
router.post("/suports/new-suport",isAuthenticated,createNewSuport);

// Get All suports
router.get("/suports",isAuthenticated,renderSuport);

// Edit suport
router.get("/suports/edit/:id",isAuthenticated,rendeEditForm);

router.put("/suports/edit-suport/:id",isAuthenticated,updateSuport);
// Delete suport
router.delete("/suports/delete/:id",isAuthenticated,deleteSuport);

module.exports = router;
