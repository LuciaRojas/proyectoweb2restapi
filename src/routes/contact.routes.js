const express = require("express");
const router = express.Router();

const {
    renderContactForm,
    createNewContact,
    updateContact,
    renderContact,
    rendeEditForm,
    deleteContact,
 
} = require("../controllers/contact.controller");

// Routes
// Helpers
const { isAuthenticated } = require("../helpers/auth");

router.get("/contacts/add/:id", isAuthenticated,renderContactForm);
//Post Contact
router.post("/contacts/new-contact", isAuthenticated,createNewContact);

// Get All Contacts
router.get("/contacts/:id", isAuthenticated,renderContact);

// Edit Contact
router.get("/contacts/edit/:id", isAuthenticated,rendeEditForm);

//router.get("/contacts/:id", isAuthenticated,rendeAllContact);

router.put("/contacts/edit-contact/:id", isAuthenticated,updateContact);
// Delete Contact
router.delete("/contacts/delete/:id", isAuthenticated,deleteContact);
module.exports = router;
