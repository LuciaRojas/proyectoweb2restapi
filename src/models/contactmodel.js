
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contact = new Schema({
  client: { type: String   ,  require:true},
  name: { type: String  ,  require:true},
  last_name: { type: String  ,  require:true},
  email: { type: String  ,  require:true},
  phone: { type: String ,  require:true },
  market_stall: { type: String ,  require:true }

});

module.exports = mongoose.model('contacts', contact);




