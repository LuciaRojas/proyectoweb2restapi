const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bcrypt = require("bcryptjs");

const user = new Schema({
  name: { type: String ,  require:true},
  last_name: { type: String  ,  require:true},
  user_name: { type: String  ,  require:true},
  password: { type: String  ,  require:true},
  type: { type: Boolean  ,  require:true}
  //name, last_name, user_name, password
});


user.methods.encryptPassword = async password => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

user.methods.matchPassword = async function(password) {
 
  return await bcrypt.compare(password, this.password);
};

/*
user.methods.decrypt = async function(password) {
 
  return await bcrypt.data(password, this.password);
};*/

module.exports = mongoose.model('users', user);
