
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const client = new Schema({
  name: { type: String  ,  require:true},
  ced: { type: String },
  pag_web: { type: String  ,  require:true},
  address: { type: String  ,  require:true},
  phone: { type: String  ,  require:true},
  sector: { type: String ,  require:true }

});

module.exports = mongoose.model('clients', client);


