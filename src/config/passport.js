const jwt = require("jsonwebtoken");
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
//const jwt = require('jsonwebtoken');
const config = require('../config');

const User = require('../models/usermodel'); 
/**
 * Si el usuario se encuentra en la base de datos lo guarda en la seccion 
 */
passport.use(new LocalStrategy({
  usernameField: 'user_name',
  passwordField: 'password'
}, async (user_name, password, done) => {
  // Match user_name's User
  const user = await User.findOne({user_name: user_name});
  if (!user) {
    return done(null, false, { message: 'Not User found.' });
  } else {
    // Match Password's User
    const match = await user.matchPassword(password);
    //console.log(match);
    //console.log("paso");
    if(match) {
   /*   const token = jwt.sign({id: user._id}, config.secret, {
        expiresIn: 60 * 60 * 24
    });
    console.log(token);
    */

      return done(null, user);
    } else {
      return done(null, false, { message: 'Incorrect Password.' });
    }
  }
}));

passport.serializeUser((user, done) => {

  done(null, user.id);
});

/**
 * busca el usuario registrado para utilizarlo y verifcar en el menu 
 */
passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    const token = jwt.sign({id: user._id}, config.secret, {
      expiresIn: 60 * 60 * 24
  });
  user.token=token;

    done(err, user);
  }).lean();
});
