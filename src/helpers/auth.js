const helpers = {};
const config = require('../config');
const jwt = require('jsonwebtoken');
/**
 * Verifica si el usuario esta auntenficado 
 */
helpers.isAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
     console.log(req.isAuthenticated()); 
  //  const decoded =  jwt.verify(user.token, config.secret);
   // req.userId = decoded.id;
    
    return next();
  }
  req.flash('error_msg', 'Not Authorized.');
  res.redirect('/users/signin');
};

module.exports = helpers;
