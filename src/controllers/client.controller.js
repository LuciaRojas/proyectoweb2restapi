const clientsCtrl = {};

const Client = require("../models/clientmodel");


// Modules
const passport = require("passport");

clientsCtrl.renderClientForm = (req, res) => {
  res.render('clients/new-client');
};
/***  valida si tiene datos del cliente   
 * @param {req} req parametros del formulario 
 * @param {*res} res 
 */
clientsCtrl.createNewClient = async(req, res) => {
  const { name, ced,pag_web,address,phone,sector } = req.body;
 
  //sector
  const errors = [];
  if (!ced) {
    errors.push({ text: "Please Write a Cedula." });
  }
  if (!name) {
    errors.push({ text: "Please Write a Name" });
  }
  if (!pag_web) {
    errors.push({ text: "Please Write a Pagina web" });
  }
  if (!address) {
    errors.push({ text: "Please Write a Adress" });
  }
  if (!phone) {
    errors.push({ text: "Please Write a Phone" });
  }
  if (!sector) {
    errors.push({ text: "Please Write a Sector" });
  }
  if (errors.length > 0) {
    res.status(422);
    res.render("clients/new-client", {
      errors,
      name, 
      ced,
      pag_web,
      address,
      phone,
      sector
    });
  } else {
    const newClient = new Client({ name, ced,pag_web,address,phone,sector  });
    //newClient.user = req.user.id;
    await newClient.save();
    res.status(201); // OK
    req.flash("success_msg", "Client Added Successfully");
    res.redirect("/clients");
  }
};

/**
 * renderisa la pagina de all-client
 * @param {*} req 
 * @param {*} res 
 */

clientsCtrl.renderClient = async(req, res) => {
   // const client = await Note.Client({ user: req.user.id }).lean();
  const clients = await Client.find().lean();
 // console.log(clients);
  res.render("clients/all-clients", {clients });
  
};

/**
 * Rendeirsa al formulario de editar
 * @param {*} req 
 * @param {*} res 
 */


clientsCtrl.rendeEditForm = async(req, res) => {

  const client = await Client.findById(req.params.id).lean();
  /*if(client.user != req.user.id){
    req.flash("error_msg", "Not Authorized");
    return res.redirect("/clients");
  }*/
  res.render("clients/edit-client",{client});



};
/**
 * Actualiza el Cliente mediante el id 
 * @param {*} req 
 * @param {*} res 
 */
clientsCtrl.updateCliente = async (req, res) => {
  const { name, ced,pag_web,address,phone,sector } = req.body;
    //sector
  await Client.findByIdAndUpdate(req.params.id, { name, ced,pag_web,address,phone,sector });
  res.status(200); // OK
  req.flash("success_msg", "Client Updated Successfully");
  res.redirect("/clients");
};
/**
 * Eliminar el Cliente mediante el id
 * @param {*} req 
 * @param {*} res 
 */
clientsCtrl.deleteClient = async(req, res) => {
  await Client.findByIdAndDelete(req.params.id);
  res.status(204);
 req.flash("success_msg", "Client Deleted Successfully");
  res.redirect("/clients");


};







module.exports = clientsCtrl;