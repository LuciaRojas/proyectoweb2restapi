//metings
const metingsCtrl = {};

const Meting = require("../models/metingmodel");
const User = require("../models/usermodel");
const Client = require("../models/clientmodel");
// Modules
const passport = require("passport");

//Renderisa a la pagina de crear nuevo meting
metingsCtrl.renderMetingForm = async(req, res) => {


  const users = await User.find().lean();
  const clients = await Client.find().lean();
  res.render('metings/new-meting', {users,clients});

};

/***  valida si tiene datos del meting   
 * @param {req} req parametros del formulario 
 * @param {*res} res 
 */
metingsCtrl.createNewMeting = async(req, res) => {
  const { title, date,user,isvirtual,client } = req.body;

  const errors = [];
  if (!title) {
    errors.push({ text: "Please Write a title." });
  }
  if (!date) {
    errors.push({ text: "Please Write a date" });
  }
  
  if (!user) {
    errors.push({ text: "Please Write a user" });
  }
  if (!client) {
    errors.push({ text: "Please Write a client" });
  }
  if (errors.length > 0) {
    res.status(422);
    res.render("metings/new-meting", {
      errors,
      title, 
      date,
      user,
      client
    });
  } else {
    const newMeting = new Meting({ title, date,user,isvirtual,client});

  
    await newMeting.save();
    res.status(201);//CREATED
      req.flash("success_msg", "Meting Added Successfully");
     res.redirect("/metings");
  }
};

/**
 * renderisa la pagina de all-Meting
 * @param {*} req 
 * @param {*} res 
 */
metingsCtrl.renderMeting = async(req, res) => {
  // const client = await Note.Client({ user: req.user.id }).lean();
 const metings = await Meting.find().lean();
 
// console.log(clients);
 res.render("metings/all-metings", {metings});
 
};

/**
 * Rendeirsa al formulario de editar
 * @param {*} req 
 * @param {*} res 
 */
metingsCtrl.rendeEditForm = async(req, res) => {

  const users = await User.find().lean();
  const clients = await Client.find().lean();

  const meting = await Meting.findById(req.params.id).lean();
  res.render("metings/edit-meting",{meting,users,clients});

};
/**
 * Actualiza el meting mediante el id 
 * @param {*} req 
 * @param {*} res 
 */
metingsCtrl.updateMeting = async (req, res) => {
  const { title, date,user,isvirtual,client  } = req.body;
    //sector
  await Meting.findByIdAndUpdate(req.params.id, { title, date,user,isvirtual,client });
  res.status(200); // OK
  req.flash("success_msg", "Meting Updated Successfully");
  res.redirect("/metings");
};
/**
 * Eliminar el meting mediante el id
 * @param {*} req 
 * @param {*} res 
 */
metingsCtrl.deleteMeting = async(req, res) => {
  await Meting.findByIdAndDelete(req.params.id);
  res.status(204);
 req.flash("success_msg", "Meting Deleted Successfully");
  res.redirect("/metings");


};


module.exports = metingsCtrl;