const usersCtrl = {};

// Models
const User = require('../models/usermodel');

// Modules
const passport = require("passport");

//Renderisa a la pagina principal
usersCtrl.renderPrincipal = (req, res) => {
  res.render('users/principal');
};

//Renderisa al formuñarop de singUp
usersCtrl.renderSignUpForm = (req, res) => {
  res.render('users/signup');
};


/***  valida si el usuario se encuentra en la base de datos y
 *  si no esta guarda el usario en la base de datos y encripta la clave 
 * @param {req} req parametros del formulario 
 * @param {*res} res 
 */
usersCtrl.singup = async (req, res) => {
  let errors = [];
  const { name, last_name,user_name, password, confirm_password } = req.body;
  if (password != confirm_password) {
    errors.push({ text: "Passwords do not match." });
  }
  if (password.length < 4) {
    errors.push({ text: "Passwords must be at least 4 characters." });
  }
  if (errors.length > 0) {
    res.status(422);
    res.render("users/signup", {
      errors,
      name,
      last_name,
      user_name,
      password,
      confirm_password
    });
  } else {
    // Look for user coincidence
    const User_name = await User.findOne({ user_name: user_name });
    if (User_name) {
      res.status(422);
      req.flash("error_msg", "The User Name is already in use.");
      res.redirect("/users/signup");
    } else {
      // Saving a New User
      type=true;
      const newUser = new User({ name, last_name,user_name, password,type });
      newUser.password = await newUser.encryptPassword(password);
      await newUser.save();
      
    /*  const token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 60 * 60 * 24 // expires in 24 hours
    });*/

      res.status(201);//CREATED
      req.flash("success_msg", "You are registered.");
      res.redirect("/users");
    }
  }
};

usersCtrl.renderSigninForm = (req, res) => {
  res.render("users/signin");
};

usersCtrl.signin = passport.authenticate("local", {
    successRedirect: "/principal",
    failureRedirect: "/users/signin",
    failureFlash: true
  });

/*
  usersCtrl.signin = (req, res) => {

  }*/

usersCtrl.logout = (req, res) => {
  req.logout();
  req.flash("success_msg", "You are logged out now.");
  res.redirect("/users/signin");
};

usersCtrl.renderUser = async(req, res) => {
   // const client = await Note.Client({ user: req.user.id }).lean();
  const users = await User.find().lean();
 
  res.render("users/all-users", {users});
  
  
  
};
/**
 * Rendeirsa al formulario de editar
 * @param {*} req 
 * @param {*} res 
 */
usersCtrl.rendeEditForm = async(req, res) => {

  const users = await User.findById(req.params.id).lean();

  res.render("users/edit-user",{users});
};
/**
 * Actualiza el usuario mediante el id 
 * @param {*} req 
 * @param {*} res 
 */
usersCtrl.updateUser = async (req, res) => {

  let errors = [];
  let { name, last_name,user_name, password, confirm_password } = req.body;
  if (password != confirm_password) {
    errors.push({ text: "Passwords do not match." });
  }
  if (password.length < 4) {
    errors.push({ text: "Passwords must be at least 4 characters." });
  }
  if (errors.length > 0) {
    req.flash("error_msg", "Invalid Password");
    res.redirect("/users");
  /*  res.render("/users/edit/"+req.params.id, {
      errors,
      name,
      last_name,
      user_name,
      password,
      confirm_password
    });*/
  } else {
    
      // update a New User
    //  type=true;
      const newUser = new User({ name, last_name,user_name, password });
      newUser.password = await newUser.encryptPassword(password);
     password=newUser.password;

      await User.findByIdAndUpdate(req.params.id, {name, last_name,user_name,password  });
      res.status(200); // OK
      req.flash("success_msg", "User Updated Successfully");
     res.redirect("/users");
    
  }


};
/**
 * Eliminar el usuario mediante el id
 * @param {*} req 
 * @param {*} res 
 */
usersCtrl.deleteUser = async(req, res) => {
  await User.findByIdAndDelete(req.params.id);
  res.status(204);
 req.flash("success_msg", "User Deleted Successfully");
  res.redirect("/users");

};

module.exports = usersCtrl;