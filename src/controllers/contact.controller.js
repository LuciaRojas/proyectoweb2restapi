const contactsCtrl = {};

const Contact = require("../models/contactmodel");


// Modules
const passport = require("passport");

//Renderisa a la pagina de crear nuevo contact
contactsCtrl.renderContactForm = (req, res) => {
    const id_client= req.params.id;
    console.log(id_client);
    res.render('contacts/new-contact',{id_client});
  };
  
/**
 * Renderiza a la pagina donde estan todos los contactos
 * @param {*} req 
 * @param {*} res 
 */
  contactsCtrl.rendeAllContact= async(req, res) => {
      
    const id_client= req.params.id;
    console.log(id_client);
    const contacts = await Contact.find({client: id_client}).lean();
    console.log(contacts);
   res.render("contacts/all-contacts", {contacts,id_client});

  }
  /***  valida si tiene datos del cliente   
 * @param {req} req parametros del formulario 
 * @param {*res} res 
 */
contactsCtrl.createNewContact = async(req, res) => {
  const { client, name,last_name,email,phone,market_stall } = req.body;
 
  //sector
  const errors = [];
  if (!name) {
    errors.push({ text: "Please Write a Name" });
  }
  if (!last_name) {
    errors.push({ text: "Please Write a Last Name" });
  }
  if (!email) {
    errors.push({ text: "Please Write a email" });
  }
  if (!phone) {
    errors.push({ text: "Please Write a Phone" });
  }
  if (!market_stall) {
    errors.push({ text: "Please Write a Market stall" });
  }
  if (errors.length > 0) {
    res.status(422);
    res.render("contacts/new-contact", {
        client,
         name,
         last_name,
         email,
         phone,
         market_stall
    });
  } else {
    const newContact = new Contact({ client, name,last_name,email,phone,market_stall  });
    //newClient.user = req.user.id;
    await newContact.save();
    res.status(201);
    req.flash("success_msg", "Contact Added Successfully");
    res.redirect("/contacts/"+client);
  }
};
/**
 * renderisa la pagina de all-suports
 * @param {*} req 
 * @param {*} res 
 */
contactsCtrl.renderContact = async(req, res) => {
 // const contacts = await Contact.find().lean();
  //console.log(contacts);
 // res.render("contacts/all-contacts", {contacts});
  
 const id_client= req.params.id;
 console.log(id_client);
 const contacts = await Contact.find({client: id_client}).lean();
 console.log(contacts);
res.render("contacts/all-contacts", {contacts,id_client});
  
};

/**
 * Rendeirsa al formulario de editar
 * @param {*} req 
 * @param {*} res 
 */

contactsCtrl.rendeEditForm = async(req, res) => {

  const contact = await Contact.findById(req.params.id).lean();
  /*if(client.user != req.user.id){
    req.flash("error_msg", "Not Authorized");
    return res.redirect("/clients");
  }*/
  res.render("contacts/edit-contact",{contact});



};
/**
 * Actualiza el contacto mediante el id 
 * @param {*} req 
 * @param {*} res 
 */
contactsCtrl.updateContact = async (req, res) => {
  const { client, name,last_name,email,phone,market_stall } = req.body;
  //sector
  await Contact.findByIdAndUpdate(req.params.id, { client, name,last_name,email,phone,market_stall});
  res.status(200); // OK
  req.flash("success_msg", "Contact Updated Successfully");
  res.redirect("/contacts/"+client);
};

/**
 * Eliminar el contact mediante el id
 * @param {*} req 
 * @param {*} res 
 */
contactsCtrl.deleteContact = async(req, res) => {
    const { id_client } = req.body;
    
    console.log(id_client);
    
        await Contact.findByIdAndDelete(req.params.id);
        res.status(204);
        req.flash("success_msg", "Contacts Deleted Successfully");
          res.redirect("/contacts/"+id_client);
};







module.exports = contactsCtrl;