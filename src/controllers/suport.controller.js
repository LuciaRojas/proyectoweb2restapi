//suports
const suportsCtrl = {};

const Suport = require("../models/suportmodel");
const Client = require("../models/clientmodel");
// Modules
const passport = require("passport");

//Renderisa a la pagina de crear nuevo sport
suportsCtrl.renderSuportForm = async(req, res) => {
  const clients = await Client.find().lean();
   
  res.render('suports/new-suport',{clients});

};
/***  valida si tiene datos del suport   
 * @param {req} req parametros del formulario 
 * @param {*res} res 
 */
suportsCtrl.createNewSuport = async(req, res) => {
  const { title, datelle,people,client,state } = req.body;

  const errors = [];
  if (!title) {
    errors.push({ text: "Please Write a title." });
  }
  if (!datelle) {
    errors.push({ text: "Please Write a datelle" });
  }
  
  if (!people) {
    errors.push({ text: "Please Write a people" });
  }
  if (!client) {
    errors.push({ text: "Please Write a client" });
  }
  if (!state) {
    errors.push({ text: "Please Write a state" });
  }
  
  if (errors.length > 0) {
    res.status(422);
    res.render("suports/new-suport", {
      errors,
      title, 
      datelle,
      people,
      client,
      state
    });
  } else {
    const newSuport = new Suport({ title, datelle,people,client,state});
    await newSuport.save();
    res.status(201);//CREATED
      req.flash("success_msg", "Suport Added Successfully");
     res.redirect("/suports");
  }
};
/**
 * renderisa la pagina de all-suports
 * @param {*} req 
 * @param {*} res 
 */
suportsCtrl.renderSuport = async(req, res) => {
  // const client = await Note.Client({ user: req.user.id }).lean();
 const suports = await Suport.find().lean();
 
// console.log(clients);
 res.render("suports/all-suports", {suports});
 
};
/**
 * Rendeirsa al formulario de editar
 * @param {*} req 
 * @param {*} res 
 */

suportsCtrl.rendeEditForm = async(req, res) => {

  const clients = await Client.find().lean();
   
  const suport = await Suport.findById(req.params.id).lean();
  res.render("suports/edit-suport",{suport,clients});



};
/**
 * Actualiza el soport mediante el id 
 * @param {*} req 
 * @param {*} res 
 */
suportsCtrl.updateSuport = async (req, res) => {
  const {  title, datelle,people,client,state  } = req.body;
    //sector
  await Suport.findByIdAndUpdate(req.params.id, { title, datelle,people,client,state });
  res.status(200); // OK
  req.flash("success_msg", "Suport Updated Successfully");
  res.redirect("/suports");
};
/**
 * Eliminar el suport mediante el id
 * @param {*} req 
 * @param {*} res 
 */
suportsCtrl.deleteSuport = async(req, res) => {
  await Suport.findByIdAndDelete(req.params.id);
  res.status(204);
  req.flash("success_msg", "Suport Deleted Successfully");
  res.redirect("/suports");


};


module.exports = suportsCtrl;